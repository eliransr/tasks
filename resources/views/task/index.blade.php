@extends('layouts.app')
@section('content')

<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

<div class ="container">
<br/><br/>
        <div class="col-5  offset-2">
            <h1>Your list tasks</h1>
        <div>
    <div class="col-10 offset-2">
      <div class="container">
          <br/><br/>
          

                @isset($filtered)
                    <a href = {{route('tasks.index')}}><H3>All tasks</H3></a>
                @else
                    <a href = {{route('myfilter')}}><h3>My tasks</h3></a>
                @endisset
                <ul class="list-group list-group-flush">
                    @foreach($tasks as $task)
                            <li class="list-group-item">
                            
                           <!-- <a href="{{route('tasks.edit' , $task->id)}}"> {{$task->title}}</a>  -->

                           {{$task->title}} <a href="{{route('tasks.edit', $task->id)}}">Edit</a> 

                           @can('admin')
                             <a href="{{route('delete', $task->id)}}">Delete</a>
                           @endcan 
                            @if ($task->status == 0)
                                @can('admin')
                                     <a href="{{route('done', $task->id)}}">Mark As done</a>
                                @endcan 
                                @else
                                     Done!
                            @endif
                                            
                            </li>    
                    @endforeach
                </ul>
        </div>
    </div>
          
    <div class ="container">
    <br>
        <div class="col-5  offset-5">
            <a href="{{route('tasks.create')}}" class=" btn btn-secondary">Add another Task to your list</a>
       
                 <script>
                    $(document).ready(function(){
                            $(":checkbox").click(function(event){
                                console.log(event.target.id)

                            $(this).attr('disabled', true);
                                alert("Finished the task");
                                $.ajax({
                                    url:"{{url('tasks')}}" + '/' + event.target.id,
                                    dataType: 'json',
                                    type:'put',
                                    contentType: 'application/json',
                                    data:  JSON.stringify({'status':event.target.checked, _token:'{{csrf_token()}}'}),
                                    processData: false,
                                    success: function( data){
                                    },
                                    error: function(errorThrown ){
                                    }
                                });               
                            });
                        });
                </script>  
        </div>
    </div>
</div>

@endsection