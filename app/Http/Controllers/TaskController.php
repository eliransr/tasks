<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;

use App\Task;
use App\User;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        if (Auth::check()) {
            // $id = Auth::id();
            // $user = User::find($id);
            // $tasks = $user->tasks;
            $tasks = Task::all();
            return view('task.index' , ['tasks'=>$tasks]);
        }

        return redirect()->intended('/home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::check()) {
            
            return view('task.create');
        }

        return redirect()->intended('/home');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::check()) {
            $this->validate($request,[
                'title'=>'required',
                ]);
            $task = new Task();
            $id = Auth::id(); //the idea of the current user
            $task->title = $request->title;
            $task->user_id = $id;
            $task->status = 0;
            $task->save();
            return redirect('tasks');
        }

        return redirect()->intended('/home');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Auth::check()) {

        }

        return redirect()->intended('/home');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::check()) {
            $task = Task::findOrFail($id);
            return view('task.edit', compact('task'));
        }

        return redirect()->intended('/home');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::check()) {
            $this->validate($request,[
                'title'=>'required',
                ]);

             //only if this todo belongs to user 
            $task = Task::findOrFail($id);            
            $task->update($request->except(['_token'])); 
            return redirect('tasks');    
        }

        return redirect()->intended('/home');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Auth::check()) {
            if (Gate::denies('manager')) {
                abort(403,"Sorry you are not allowed to delete tasks"  );
                    
            }
            $task = Task::findOrFail($id);
            $task->delete();
            return redirect('tasks');    
        }

        return redirect()->intended('/home');

    }
        // change the status function
    public function change_status($id,$status)
    {
        return view('tasks.index', ['id'=>$id, 'status'=>$status]);
    }

            // cahnge the done 
    public function done($id)
    {
        // //only if this done belongs to user 
        if (Gate::denies('admin')) {
            abort(403,"You are not allowed to mark tasks as dome..");
         }          
        $task = Task::findOrFail($id);            
        $task->status = 1; 
        $task->save();
        return redirect('tasks');    
    }    
    public function indexFiltered()
    {
        $id = $id = Auth::id(); //the current user 
        $tasks = Task::where('user_id', $id)->get();
        $filtered = 1; //this is to mark the view to show link to all tasks 
        return view('task.index', compact('tasks','filtered'));
    }    

}
