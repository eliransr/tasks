<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
     //database fields to be allwed for massive assigment
     protected $fillable =[
        'title',
        
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
